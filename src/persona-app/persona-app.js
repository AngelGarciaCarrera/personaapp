import { LitElement, html } from "lit-element";
import '../persona-header/persona-header.js';

class PersonaApp extends LitElement {
  render() {
    return html`
      <persona-header></persona-header>
    `;
  }
}

customElements.define('persona-app', PersonaApp);
